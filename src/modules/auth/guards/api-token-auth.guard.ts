import {
  Injectable,
  CanActivate,
  Inject,
  ExecutionContext,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';

import { AuthRepositoryTokens } from '@/modules/auth/db/providers/repository-tokens.enum';
import { IApiTokesRepository } from '@/modules/auth/db/repositories/api-tokens.repository.interface';
import { CustomAuthHeaders } from '@/modules/auth/enums/custom-auth-headers.enum';
import { AuthError } from '@/modules/auth/errors/auth.errors';

@Injectable()
export class ApiTokenAuthGuard implements CanActivate {
  private readonly logger = new Logger(ApiTokenAuthGuard.name);

  constructor(
    /* c8 ignore next 3 */
    private readonly env: ConfigService,
    @Inject(AuthRepositoryTokens.API_TOKENS_REPOSITORY)
    private readonly apiTokensRepository: IApiTokesRepository,
  ) {}

  /**
   * O token pode ser enviado de duas formas:
   * 1. No header x-api-token
   * 2. No header Authorization com o valor Bearer <token>
   */
  private getApiToken(request: Request): string | undefined {
    let apiToken = request.headers[CustomAuthHeaders.API_TOKEN];

    if (!apiToken) {
      const authorization = request.headers.authorization;

      if (authorization) {
        const [bearer, token] = authorization.split(' ');

        if (bearer === 'Bearer') apiToken = token;
      }

      if (!apiToken)
        apiToken = request.query[CustomAuthHeaders.API_TOKEN] as string;
    }

    return apiToken as string;
  }

  private validateApiToken(apiToken: string): boolean {
    const adminToken = this.env.get<string>('ADMIN_AUTH_TOKEN');

    if (apiToken === adminToken) return true;

    const novoPortalApiToken = this.env.get<string>(
      'NOVO_PORTAL_API_TOKEN',
    ) as string;

    if (novoPortalApiToken && apiToken === novoPortalApiToken) return true;

    const monitorApiToken = this.env.get<string>(
      'MONITOR_SEFAZ_API_TOKEN',
    ) as string;

    if (monitorApiToken && apiToken === monitorApiToken) return true;

    const wordpressApiToken = this.env.get<string>('WORDPRESS_API_TOKEN');

    if (wordpressApiToken && apiToken === wordpressApiToken) return true;

    return false;
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    const apiToken = this.getApiToken(request);
    delete request.query[CustomAuthHeaders.API_TOKEN];

    if (!apiToken) throw new AuthError.ApiTokenNotProvided(this.logger);

    const isTokenValid = this.validateApiToken(apiToken);

    if (!isTokenValid) throw new AuthError.Unauthorized(this.logger);

    // Consulta de tokens no banco de dados desabilitada por enquanto
    // Será habilitada quando o CRUD de api tokens for implementado
    // const existingToken = await this.apiTokensRepository.findByToken(
    //   apiToken as string,
    // );
    //
    // if (!existingToken) throw new AuthError.Unauthorized(this.logger);

    return true;
  }
}
