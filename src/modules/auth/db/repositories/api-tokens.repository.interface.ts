import { ApiToken } from '@/modules/auth/entities/api-token-entity';

export interface IApiTokesRepository {
  findByToken(token: string): Promise<ApiToken | null>;
}
