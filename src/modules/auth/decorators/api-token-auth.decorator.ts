import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';

import { SwaggerApiTokenHeader } from '@/modules/auth/decorators/api-token-required.decorator';
import { ApiTokenAuthGuard } from '@/modules/auth/guards/api-token-auth.guard';

export function APITokenProtected() {
  return applyDecorators(
    UseGuards(ApiTokenAuthGuard),
    SwaggerApiTokenHeader(),
    ApiBearerAuth(),
  );
}
