// PF: Os repositories terão somente testes de integração, pois não faz sentido mockar a conexão com o banco de dados em um nivel tão baixo.
// PF: Os testes de integração são feitos utilizando o banco de dados real, porém em um banco de dados separado do banco de dados de produção. O mesmo deve ser informado no arquivo .env.test

it('teste temporário', async () => {
  expect(1).toBe(1);
});
