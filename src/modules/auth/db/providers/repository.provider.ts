import { Provider } from '@nestjs/common';

import { AuthRepositoryTokens } from '@/modules/auth/db/providers/repository-tokens.enum';
import { MongoApiTokensRepository } from '@/modules/auth/db/repositories/implementations/mongo-api-tokens.repository';

export const authRepositoryProviders: Provider[] = [
  {
    provide: AuthRepositoryTokens.API_TOKENS_REPOSITORY,
    useClass: MongoApiTokensRepository,
  },
];
