/* eslint-disable no-unused-expressions */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';

import { AllowedNodeEnv } from '@/config/env.config';
import { CustomValidationError } from '@/validation.pipe';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const logger = new Logger(this.constructor.name);

    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const shouldLogError = [
      AllowedNodeEnv.DEVELOPMENT,
      AllowedNodeEnv.SANDBOX,
      AllowedNodeEnv.PRODUCTION,
    ].includes(process.env.NODE_ENV as AllowedNodeEnv);

    const requestData = {
      body: request.body,
      query: request.query,
      params: request.params,
      headers: request.headers,
    };

    const env = process.env.NODE_ENV as AllowedNodeEnv;

    let status;

    if (exception instanceof HttpException) {
      status = exception.getStatus();
      if (exception instanceof CustomValidationError) {
        const exceptionResponse: any = exception.getResponse();

        shouldLogError &&
          logger.error(`Request: ${request.method} ${request.url}`, {
            error: exception.message,
            requestData,
          });

        return response.status(status).json(exceptionResponse);
      }
    } else {
      status = HttpStatus.INTERNAL_SERVER_ERROR;
      shouldLogError &&
        logger.error(`Request: ${request.method} ${request.url}`, {
          error: exception.message,
          stack: exception.stack,
          requestData,
        });
    }

    if (
      status === HttpStatus.INTERNAL_SERVER_ERROR &&
      env === AllowedNodeEnv.DEVELOPMENT
    ) {
      return response.status(status).json({
        message: exception.message,
        stack: exception.stack,
      });
    }

    return response.status(status).json({
      message:
        status === HttpStatus.INTERNAL_SERVER_ERROR
          ? 'Erro interno do servidor'
          : exception.message,
    });
  }
}
