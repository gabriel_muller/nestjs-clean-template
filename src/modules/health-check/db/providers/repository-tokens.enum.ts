// PF: Tokens utilizados para injetar o repositório
// PF: É utilizado um Enum para facilitar a manutenção e evitar erros de digitação
export enum HealthCheckReporitoryTokens {
  EXAMPLE_DOCUMENTS_REPOSITORY = 'EXAMPLE_DOCUMENTS_REPOSITORY',
}
