import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

// PF: Model do Mongoose
// PF: Não precisa espelhar a entidade de domínio, mas caso não tenha o mesmo formato, deverá ser feito um mapeamento ao retornar para o domínio
@Schema()
export class ExampleDocumentModel {
  @Prop()
  name: string;

  @Prop()
  description: string;
}

export type ExampleDocumentDocument = HydratedDocument<ExampleDocumentModel>;

export const ExampleDocumentSchema =
  SchemaFactory.createForClass(ExampleDocumentModel);
