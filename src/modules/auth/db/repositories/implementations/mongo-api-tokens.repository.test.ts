import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';

import { ConfigModuleFactory } from '@/config/env.config';
import { mockApiTokenEntity } from '@/modules/auth/__mocks__/entities';
import {
  ApiTokenModel,
  ApiTokenSchema,
} from '@/modules/auth/db/models/api-tokenmodel';
import { MongoApiTokensRepository } from '@/modules/auth/db/repositories/implementations/mongo-api-tokens.repository';

describe('MongoApiTokensRepository', () => {
  let apiTokenModel = Model<ApiTokenModel>;
  let sut: MongoApiTokensRepository;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MongoApiTokensRepository],
      imports: [
        ConfigModuleFactory.create(),
        MongooseModule.forRoot(process.env.MONGODB_URI),
        MongooseModule.forFeature([
          { name: ApiTokenModel.name, schema: ApiTokenSchema },
        ]),
      ],
    }).compile();

    apiTokenModel = module.get<Model<ApiTokenModel>>(
      getModelToken(ApiTokenModel.name),
    );
    sut = module.get<MongoApiTokensRepository>(MongoApiTokensRepository);
  });

  afterEach(async () => {
    await apiTokenModel.deleteMany({});
    vitest.resetAllMocks();
  });

  beforeEach(async () => {
    await apiTokenModel.deleteMany({});
  });

  it('deve encontrar um api token existente', async () => {
    const { id: _id, ...mockTokenData } = mockApiTokenEntity();

    await apiTokenModel.create({ ...mockTokenData });

    const token = await sut.findByToken(mockTokenData.token);

    expect(token).toBeTruthy();
    expect(token).toEqual(
      expect.objectContaining({
        token: mockTokenData.token,
        nome: mockTokenData.nome,
        ativo: mockTokenData.ativo,
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      }),
    );
  });

  it('deve retornar null se não encontrar um api token', async () => {
    const mockTokenData = mockApiTokenEntity();

    const token = await sut.findByToken(mockTokenData.token);

    expect(token).toBeNull();
  });
});
