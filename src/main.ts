import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from '@/app.module';
import { setupGlobalFilters, setupGlobalPipes } from '@/config/globals.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger('Bootstrap');
  const config = app.get(ConfigService);

  const apiPrefix = config.get<string>('API_PREFIX');
  const port = config.get<number>('PORT');
  const env = config.get<string>('NODE_ENV');

  const docsURL = `${apiPrefix}/docs`;

  app.setGlobalPrefix(apiPrefix);

  // Configurações do Swagger
  const swaggerConfig = new DocumentBuilder()
    .setTitle('Tecno Gateway')
    .setDescription('API Gateway interno da TecnoSpeed')
    .addBearerAuth({
      type: 'http',
      in: 'header',
      description: 'API Token',
    })
    .build();

  const document = SwaggerModule.createDocument(app, swaggerConfig);

  SwaggerModule.setup(docsURL, app, document, {
    customSiteTitle: 'Swagger - Tecno Gateway',
    customCss: '.models { display: none !important; }',
    swaggerOptions: {
      persistAuthorization: true,
    },
  });

  app.enableCors();

  setupGlobalPipes({ app });
  setupGlobalFilters({ app });

  await app.listen(port, () => {
    logger.log(`Ambiente: ${env}`);
    logger.log(`Docs: http://localhost:${port}/${docsURL}`);
    logger.log(`API executando na porta ${port}`);
  });
}
bootstrap();
