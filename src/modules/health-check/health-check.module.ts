import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import { HealthCheckController } from '@/modules/health-check/controllers/health-check.controller';
import {
  ExampleDocumentModel,
  ExampleDocumentSchema,
} from '@/modules/health-check/db/models/example-document.model';
import { healthCheckRepositoryProviders } from '@/modules/health-check/db/providers/repository.providers';
import { GetStatusUsecase } from '@/modules/health-check/usecases/get-status.usecase';

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature([
      { name: ExampleDocumentModel.name, schema: ExampleDocumentSchema },
    ]),
  ],
  controllers: [HealthCheckController],
  providers: [...healthCheckRepositoryProviders, GetStatusUsecase],
  exports: [...healthCheckRepositoryProviders],
})
export class HealthCheckModule {}
