import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';

export enum AllowedNodeEnv {
  DEVELOPMENT = 'development',
  TEST = 'test',
  PRODUCTION = 'production',
  SANDBOX = 'sandbox',
}

const DEFAULT_PORT = 3000;
const DEFAULT_API_PREFIX = 'api/v1';
const DEFAULT_NODE_ENV = AllowedNodeEnv.DEVELOPMENT;

// const isTestEnv = process.env.NODE_ENV === AllowedNodeEnv.TEST;

export const validationSchema = Joi.object({
  PORT: Joi.number().default(DEFAULT_PORT),
  NODE_ENV: Joi.string()
    .valid(...Object.values(AllowedNodeEnv))
    .default(DEFAULT_NODE_ENV),
  API_PREFIX: Joi.string().default(DEFAULT_API_PREFIX),
});

const env = process.env.NODE_ENV || AllowedNodeEnv.DEVELOPMENT;
export class ConfigModuleFactory {
  static create() {
    return ConfigModule.forRoot({
      envFilePath: `.env.${env}`,
      isGlobal: true,
      validationSchema,
    });
  }
}
