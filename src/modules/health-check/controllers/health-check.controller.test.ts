import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import request from 'supertest';

import { AppModule } from '@/app.module';
import { ConfigModuleFactory } from '@/config/env.config';
import { setupGlobalFilters, setupGlobalPipes } from '@/config/globals.config';

describe('Health Check E2E', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule, ConfigModuleFactory.create()],
    }).compile();

    app = module.createNestApplication();

    setupGlobalPipes({ app });
    setupGlobalFilters({ app });

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('GET /status', () => {
    it('deve retornar ok no sucesso', async () => {
      const healthCheckResponse = await request(app.getHttpServer())
        .get('/status')
        .send()
        .expect(200);

      expect(healthCheckResponse.body).toHaveProperty('status', 'OK');
    });
  });
});
