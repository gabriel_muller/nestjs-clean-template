import {
  ForbiddenException,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';

import { CustomAuthHeaders } from '@/modules/auth/enums/custom-auth-headers.enum';

export namespace AuthError {
  export enum Message {
    FORBIDDEN = 'Acesso negado',
    UNAUTHORIZED = 'Não autorizado',
    API_TOKEN_NOT_PROVIDED = `É necessário enviar o apiToken no header ${CustomAuthHeaders.API_TOKEN}`,
  }

  /* c8 ignore start */
  export class Forbidden extends ForbiddenException {
    constructor(logger: Logger) {
      const message = AuthError.Message.FORBIDDEN;
      logger.warn(message);
      super(message);
    }
  }
  /* c8 ignore end */

  export class Unauthorized extends UnauthorizedException {
    constructor(logger: Logger) {
      const message = AuthError.Message.UNAUTHORIZED;
      logger.warn(message);
      super(message);
    }
  }

  export class ApiTokenNotProvided extends UnauthorizedException {
    constructor(logger: Logger) {
      const message = AuthError.Message.API_TOKEN_NOT_PROVIDED;
      logger.warn(message);
      super(message);
    }
  }
}
