import { FindByIdDTO } from '@/modules/health-check/db/repositories/dtos/find-by-id.dto';
import { ExampleDocument } from '@/modules/health-check/entities/example-document.entity';

// PF: Interface do repositorio de exemplo
// PF: Apesar da implementação usar MongoDB, a interface não deve saber disso
// PF: O tipo retornado pela interface é baseado nas entidades do domínio, sem dependência de tecnologia
export interface IExampleDocumentsRepository {
  findById(params: FindByIdDTO): Promise<ExampleDocument | null>;
}
