import { applyDecorators } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';

import { CustomAuthHeaders } from '@/modules/auth/enums/custom-auth-headers.enum';

export function SwaggerApiTokenHeader() {
  return applyDecorators(
    ApiHeader({ name: CustomAuthHeaders.API_TOKEN, description: 'API Token' }),
  );
}
