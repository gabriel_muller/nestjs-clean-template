import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ExampleDocumentModel } from '@/modules/health-check/db/models/example-document.model';
import { FindByIdDTO } from '@/modules/health-check/db/repositories/dtos/find-by-id.dto';
import { IExampleDocumentsRepository } from '@/modules/health-check/db/repositories/example-documents.repository.interface';
import { ExampleDocument } from '@/modules/health-check/entities/example-document.entity';

// PF: Implementação do repository para MongoDB
// PF: A model é injetada pelo NestJS
// PF: Podem existir reposiórios com mais de uma model injetada, caso seja necessário
export class MongoExampleDocumentsRepository
  implements IExampleDocumentsRepository
{
  constructor(
    /* c8 ignore next 2 */
    @InjectModel(ExampleDocumentModel.name)
    private readonly model: Model<ExampleDocumentModel>,
  ) {}

  /* c8 ignore next 11 */
  async findById({ id }: FindByIdDTO): Promise<ExampleDocument | null> {
    const response = await this.model.findById(id).exec();

    if (!response) return null;

    return {
      id: response._id.toString(),
      name: response.name,
      description: response.description,
    };
  }
}
