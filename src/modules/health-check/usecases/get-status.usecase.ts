import { Injectable } from '@nestjs/common';

import { HealthCheckResponse } from '@/modules/health-check/usecases/dtos/health-check-response.dto';

@Injectable()
export class GetStatusUsecase {
  async execute(): Promise<HealthCheckResponse> {
    return {
      status: 'OK',
    };
  }
}
