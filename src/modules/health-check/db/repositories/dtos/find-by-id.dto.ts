// PF: Formato esperado pelo método findById do repositório
// PF: Apesar de ser o mesmo do usecase nesse caso, é recomendado criar um novo DTO para evitar acoplamento. Não são todos casos em que os DTOs serão iguais.
export type FindByIdDTO = {
  id: string;
};
